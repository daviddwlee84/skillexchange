package edu.ntust.skillexchange;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Da-Wei on 2015/8/5.
 */
public class UserLocalStore {

    public static final String SP_NAME = "userDetails";
    SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context) {
        userLocalDatabase = context.getSharedPreferences(SP_NAME, 0);
    }

    public void storeUserData(User user) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("name", user.name);
        spEditor.putString("password", user.password);
        spEditor.putString("schoolEmail", user.schoolEmail);
        spEditor.putString("school", user.school);
        spEditor.putString("grade", user.grade);
        spEditor.putString("email", user.email);
        spEditor.commit();
    }

    public User getLoggedInUser() {
        String name = userLocalDatabase.getString("name", "");
        String password = userLocalDatabase.getString("passwrod", "");
        String schoolEmail = userLocalDatabase.getString("schoolEmail", "");
        String school = userLocalDatabase.getString("school", "");
        String grade = userLocalDatabase.getString("grade", "");
        String email = userLocalDatabase.getString("email", "");

        User storedUser = new User (name, school, grade, schoolEmail, email, password);
        return storedUser;
    }

    public void setUserLoggedIn(boolean loggedIn){
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putBoolean("loggedIn", loggedIn);
        spEditor.commit();
    }

    public boolean getUserLoggedIn(){
        if(userLocalDatabase.getBoolean("loggedIn", false) == true){
            return true;
        }
        else{
            return false;
        }
    }

    public void clearUserData(){
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }
}
