package edu.ntust.skillexchange;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

public class MessageActivity extends AppCompatActivity {

    private LinearLayout MessageContent;
    private ListView MessageMenu;
    // 記錄被選擇的選單指標用
    private int CurrentMenuItemPosition = -1;
    // 選單項目
    public static final String[] message_list = new String[]{
            "邱懷民", "李大為", "張子晏", "XXX", "OOO", "WWW", "AAA", "BBB", "CCC", "DDD"
            , "EEE", "FFF", "GGG"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        setMessageMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setMessageMenu() {
        // 定義新宣告的兩個物件：選項清單的 ListView 以及 Drawer內容的 LinearLayout
        MessageMenu = (ListView) findViewById(R.id.list_message);
        MessageContent = (LinearLayout) findViewById(R.id.message_layout);

        // 當清單選項的子物件被點擊時要做的動作
        MessageMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                selectMenuItem(position);
                switch(position){
                    //開始聊天
                }
            }
        });
        // 設定清單的 Adapter，這裡直接使用 ArrayAdapter<String>
        MessageMenu.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.message_list_item,  // 選單物件的介面
                message_list               // 選單內容
        ));
    }

    private void selectMenuItem(int position) {
        CurrentMenuItemPosition = position;

        // 將選單的子物件設定為被選擇的狀態
        MessageMenu.setItemChecked(position, true);
    }
}
