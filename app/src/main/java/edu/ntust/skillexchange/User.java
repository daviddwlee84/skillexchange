package edu.ntust.skillexchange;

/**
 * Created by Da-Wei on 2015/8/5.
 */
public class User {

    String name, school, grade, schoolEmail, email, password;

    public User (String name, String school,String grade, String schoolEmail, String email, String password){
        this.name = name;
        this.school = school;
        this.grade = grade;
        this.schoolEmail = schoolEmail;
        this.email = email;
        this.password = password;
    }

    public User(String schoolEmail, String password){
        this.schoolEmail = schoolEmail;
        this.password = password;
        this.school = "";
        this.email = "";
        this.grade = "";
        this.name = "";
    }

}
