package edu.ntust.skillexchange;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    Button butRegister;
    EditText editName, editSchool, editGrade, editSchoolEmail, editEmail, editPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editName = (EditText) findViewById(R.id.editName);
        editSchool = (EditText) findViewById(R.id.editSchool);
        editGrade = (EditText) findViewById(R.id.editGrade);
        editSchoolEmail = (EditText) findViewById(R.id.editSchoolEmail);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword = (EditText) findViewById(R.id.editPassword);

        butRegister = (Button) findViewById(R.id.butRegister);

        butRegister.setOnClickListener(this);
    }


    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.butRegister:

                String name = editName.getText().toString();
                String school = editSchool.getText().toString();
                String grade = editGrade.getText().toString();
                String schoolEmail = editSchoolEmail.getText().toString();
                String email = editEmail.getText().toString();
                String password = editPassword.getText().toString();

                User registeredData = new User(name, school, grade, schoolEmail, email, password);

                break;
        }
    }
}
