package edu.ntust.skillexchange;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class ShareActivity extends AppCompatActivity {

    private LinearLayout ShareContent;
    private ListView ShareMenu;
    // 記錄被選擇的選單指標用
    private int CurrentMenuItemPosition = -1;
    // 選單項目
    public static final String[] share_list = {
            "111111111", "222222222", "333333", "44444444444", "55555555",
            "66", "77", "888", "9", "10", "11", "12"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        setShareMenu();
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void setShareMenu() {
        // 定義新宣告的兩個物件：選項清單的 ListView 以及 Drawer內容的 LinearLayout
        ShareMenu = (ListView) findViewById(R.id.list_share);
        ShareContent = (LinearLayout) findViewById(R.id.share_layout);

        // 當清單選項的子物件被點擊時要做的動作
        ShareMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                selectMenuItem(position);
                switch(position){
                    //讀取文章內容
                }
            }
        });
        // 設定清單的 Adapter，這裡直接使用 ArrayAdapter<String>
        ShareMenu.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.share_list_item,  // 選單物件的介面
                share_list                  // 選單內容
        ));
    }

    private void selectMenuItem(int position) {
        CurrentMenuItemPosition = position;

        // 將選單的子物件設定為被選擇的狀態
        ShareMenu.setItemChecked(position, true);
    }
}
