package edu.ntust.skillexchange;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login extends AppCompatActivity implements View.OnClickListener {


    Button butLogin, butGoRegister;
    EditText editUsername, editPassword;

    UserLocalStore userLocalStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        editUsername = (EditText) findViewById(R.id.editUsername);
        editPassword = (EditText) findViewById(R.id.editPassword);
        butLogin = (Button) findViewById(R.id.butLogin);
        butGoRegister = (Button) findViewById(R.id.butGoRegister);

        butLogin.setOnClickListener(this);
        butGoRegister.setOnClickListener(this);

        userLocalStore = new UserLocalStore(this);
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.butLogin:

                User user = new User(null, null);

                userLocalStore.storeUserData(user);
                userLocalStore.setUserLoggedIn(true);
                break;

            case R.id.butGoRegister:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

}
