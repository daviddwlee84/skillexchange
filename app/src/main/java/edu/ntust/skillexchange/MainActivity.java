package edu.ntust.skillexchange;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.parse.Parse;
import com.parse.ParseInstallation;



public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView gift;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private LinearLayout mLlvDrawerContent;
    private ListView mLsvDrawerMenu;
    // 記錄被選擇的選單指標用
    private int mCurrentMenuItemPosition = -1;
    // 選單項目
    public static final String[] MENU_ITEMS = new String[]{
            "使用者", "關於", "登出"
    };

    Button btnChat, btnShare, btnGroup;

    UserLocalStore userLocalStore; //儲存登入資訊

    final boolean canMatch = true;//判斷是否可配對

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gift = (ImageView)findViewById(R.id.gift); //禮物圖片

        btnChat = (Button)findViewById(R.id.btnChat); //對話框
        btnShare = (Button)findViewById(R.id.btnShare); //心得分享
        //btnGroup = (Button)findViewById(R.id.btnGroup); //社團

        userLocalStore = new UserLocalStore(this);

        //Parse initializing 這兩行會出錯！！
        //Parse.initialize(this, "lu5PacUvs0luUkTNlB35TmPxoQL36Mljy8js8Tyn", "zxldOS44CDtIBHGksOqNyyumL5Dq0qE92F2A9xbq");
        //ParseInstallation.getCurrentInstallation().saveInBackground();


        gift.setImageResource(R.mipmap.present);
        gift.setOnClickListener(this);

        btnChat.setOnClickListener(this);
        //btnGroup.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drw_layout);
        // 設定 Drawer 的影子
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,    // 讓 Drawer Toggle 知道母體介面是誰
                R.drawable.ic_drawer, // Drawer 的 Icon
                R.string.open_left_drawer, // Drawer 被打開時的描述
                R.string.close_left_drawer // Drawer 被關閉時的描述
        ) {
            //被打開後要做的事情
            @Override
            public void onDrawerOpened(View drawerView) {
                // 將 Title 設定為自定義的文字
                getSupportActionBar().setTitle(R.string.open_left_drawer);
                setDrawerMenu();
            }

            //被關上後要做的事情
            @Override
            public void onDrawerClosed(View drawerView) {
                // 將 Title 設定回 APP 的名稱
                getSupportActionBar().setTitle(R.string.app_name);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        //顯示 Up Button (位在 Logo 左手邊的按鈕圖示)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_list_white_24dp);
        //打開 Up Button 的點擊功能
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(authenticate() == true){
            //有成功登入
        }
        else {
            startActivity(new Intent(this, Login.class));
        }
    }

    private boolean authenticate() {
        return userLocalStore.getUserLoggedIn();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnChat:
                startActivity(new Intent(getApplicationContext(), MessageActivity.class));
                break;

            case R.id.btnGroup:
                break;

            case R.id.btnShare:
                startActivity(new Intent(getApplicationContext(), ShareActivity.class));
                break;

            case R.id.gift:
                if (canMatch) { //判斷是否可以配對
                    gift.setImageResource(R.mipmap.present_open);


                    AlertDialog.Builder match = new AlertDialog.Builder(MainActivity.this);

                    match.setTitle("配對資訊");
                    String match_info = "姓名：XXX\n就讀學校：台科大\n專長：XX、XXX";
                    match.setMessage(match_info);
                    match.setPositiveButton("配對", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    match.setNegativeButton("不配對", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    match.setNeutralButton("換一個", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    match.show();
                }
                break;
            }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void setDrawerMenu() {
        // 定義新宣告的兩個物件：選項清單的 ListView 以及 Drawer內容的 LinearLayout
        mLsvDrawerMenu = (ListView) findViewById(R.id.lsv_drawer_menu);
        mLlvDrawerContent = (LinearLayout) findViewById(R.id.llv_left_drawer);

        // 當清單選項的子物件被點擊時要做的動作
        mLsvDrawerMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                selectMenuItem(position);
                switch(position){
                    case 0:
                        startActivity(new Intent(getApplicationContext(), UserInfoActivity.class));
                        break;
                    case 1:
                        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                        dialog.setTitle("關於");
                        dialog.setMessage("開發者：李大為，邱懷民\n發表日期：2015/9/30");
                        dialog.show();
                        break;
                    case 2:
                        userLocalStore.clearUserData();
                        userLocalStore.setUserLoggedIn(false);

                        startActivity(new Intent(getApplicationContext(), Login.class));
                        break;
                }
            }
        });
        // 設定清單的 Adapter，這裡直接使用 ArrayAdapter<String>
        mLsvDrawerMenu.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.drawer_menu_item,  // 選單物件的介面
                MENU_ITEMS                  // 選單內容
        ));
    }

    private void selectMenuItem(int position) {
        mCurrentMenuItemPosition = position;

        // 將選單的子物件設定為被選擇的狀態
        mLsvDrawerMenu.setItemChecked(position, true);

        // 關掉 Drawer
        mDrawerLayout.closeDrawer(mLlvDrawerContent);
    }
}
